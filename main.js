import KeyboardSpacer from 'react-native-keyboard-spacer';
import Exponent from 'exponent';
import React from 'react';
import Post from './post.js';
import Button from 'react-native-button';
import Modal from 'react-native-simple-modal';
import CheckBox from 'react-native-checkbox';
import AutogrowInput from 'react-native-autogrow-input';
import Drawer from 'react-native-drawer';
import TestScreen from './TestScreen';

import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  div,
  ScrollView,
  ListView,
  TextInput,
  TouchableOpacity
} from 'react-native';

import {
  createRouter,
  NavigationProvider,
  StackNavigation,
} from '@exponent/ex-navigation';

var posts = [
        {name: "Zebra",
          index: 5,
          upvotes: 2, 
          tags: "Lonely, Sad, Stressed", 
          postText: "I’ve been feeling very stressed-out and isolated throughout this school year. I feel as if I’m struggling to make close friends more than other people around me and it’s really stressing me out because I don’t feel as if I have anyone to go to about the problems I’m having. I miss the closeness I had with my high school friends and I just wish I could go back to when I had all of them around me.",
          comments: [
            {
              index:1,
              text: "Missing your high school friends is totally natural, but it fades with time. Try joining clubs or reaching out to people in your dorm at mealtimes..",
              upvotes: 5,
            },
            {
              index:1,
              text: "You're not the only one feeling this! I feel like I haven't found really close friends either.",
              upvotes: 5,
            },
            {
              index:1,
              text: "You'll be home in familiar surroundings soon, and next quarter is a great time to try being more social.",
              upvotes: 5,
            },
            {
              index:1,
              text: "Reach out to your RAs if you need to talk to someone right away. I’d be down to meet up some time—I’ll send you a request.",
              upvotes: 5,
            }, 
          ],
        },
         {name: "Squirrel",
          index: 4,
          upvotes: 7,
          tags: "Stressed, Math 51, Frosh, underprepared...", 
          postText: "Math 51 is killing my soul, I work on it all the time but I still feel like I don’t understand any of it. I don’t have a group to work on it with and office hours don’t feel like they’re helping. What do I do?",
          comments: [
            {
              index:1,
              text: "Every dorm has at least 2 people in the class! Reach out to your dormmates to check psets and go over material",
              upvotes: 5,
            },
            {
              index:1,
              text: "Try attending a different TA’s section and see if it makes more sense. Good luck!",
              upvotes: 5,
            },
            {
              index:1,
              text: "Honestly, everyone struggles through that class. Hang in there, do your best, and remember that Cs get degrees!",
              upvotes: 5,
            },
            {
              index:1,
              text: "Even if things don’t get better this quarter, plenty of people retake 51 and do much better with the extra experience.",
              upvotes: 5,
            }, 
          ],
        },
         {name: "Penguin",
          index: 3,
          upvotes: 3,
          tags: "Divorced, Overwhelmed, Family", 
          postText: "I just talked to my parents and they’re getting a divorce and I don’t know what to do. I’m feeling extremely overwhelmed with school already and this has just been the cherry on top of a horrible quarter. I can’t imagine my family getting split up and besides that I’m really worried about how my sister will take the news and how I’m going to support her if she takes it badly",
          comments: [
            {
              index:1,
              text: "Wow, that's really rough! If things get really bad, try calling the Bridge and talking it out",
              upvotes: 5,
            },
            {
              index:1,
              text: "Something similar happened to me during my freshman year. It really sucked, but at the end of the day, they're still your parents and they care about you a lot.",
              upvotes: 5,
            },
            {
              index:1,
              text: "Take some time for self-care this weekend. Hiking can be a great outlet when I get overwhelmed. You and your sister can support each other through this difficult time.",
              upvotes: 5,
            },
            {
              index:1,
              text: "I'm so sorry things are so tough right now, OP!",
              upvotes: 5,
            }, 
          ],
        },
        {name: "Moose",
          index: 2,
          upvotes: 14,
          tags: "Anxiety, Counseling", 
          postText:"I’ve been having a lot of issues with my anxiety recently and I feel like I’m being an annoyance to my friends, but I also don’t feel comfortable trying to go get actual counseling because I feel like that would be admitting to the world that I have a real problem. I’ve always been able to manage my anxiety on my own in the past and I don’t feel as if that should change now.",
          comments: [
            {
              index:1,
              text: "There's no shame in reaching out for professional support!!!",
              upvotes: 5,
            },
            {
              index:1,
              text: "I understand how hard it can be to face, but admitting that your feelings are a real problem can go a long way in getting them under control",
              upvotes: 5,
            },
            {
              index:1,
              text: "I'm sure you don't annoy your friends--they probably just want to help! By seeking professional resources, you're taking back control and helping yourself in the long run.",
              upvotes: 5,
            },
            {
              index:1,
              text: "Try calling the Bridge or making a CAPS appointment. If it's not for you, it's not for you, but you ought to try it out",
              upvotes: 5,
            }, 
          ],
        },
        { name: "Lion",
          index: 1, 
          upvotes: 9,
          tags: "Drama, Friendship, Freshman Dorm", 
          postText:"I really loved my freshman dorm at the beginning of the year but now I’m getting really tired of the people in it. Everyone feels so petty and clique-y and I don’t like being a part of any of the drama. I just wish everyone could get along.",
          comments: [
            {
              index:1,
              text: "Just stay out of the fray, and I'm sure you'll find other people who are over the drama",
              upvotes: 5,
            },
            {
              index:1,
              text: "Ugh, yeah, I thought we were over high school nonsense",
              upvotes: 5,
            },
            {
              index:1,
              text: "Are your RAs encouraging it? Maybe you can talk things over at a house meeting or something",
              upvotes: 5,
            },
            {
              index:1,
              text: "It can be hard to live with total strangers for a whole year, but most of the drama fades after fall quarter. Everyone will get settled soon",
              upvotes: 5,
            }, 
          ],
        },
         {name: "Giraffe",
          index: 0, 
          upvotes: 4,
          tags: "Major, Chem", 
          postText: "I’m feeling very conflicted about what I should major in. I thought I wanted to be a bio major but I’ve realized that I hate chemistry but I don’t know what else I could possibly major in. I feel very lost and I feel like my life has become very directionless. Help!",
          comments: [
            {
              index:1,
              text: "Look through explore courses and find some classes that look interesting. It's never too late to change majors!",
              upvotes: 5,
            },
            {
              index:1,
              text: "Don't make yourself miserable in classes you hate! If you're really lost, ask friends for recommendations and don't be afraid to explore",
              upvotes: 5,
            },
            {
              index:1,
              text: "I went from a Chem major to a much-happier Comparative Lit major, and I've never regretted it! Passions and interests change in college, and there's no shame in doing what you love",
              upvotes: 5,
            },
            {
              index:1,
              text: "Chem tends to have that effect on people. Get out while you can!!",
              upvotes: 5,
            }, 
          ],
       },
      ];

const Router = createRouter(() => ({
  test: () => TestScreen,
  home: () => HomeScreen,
}));

class App extends React.Component {
   render() {
    /**
      * NavigationProvider is only needed at the top level of the app,
      * similar to react-redux's Provider component. It passes down
      * navigation objects and functions through context to children.
      *
      * StackNavigation represents a single stack of screens, you can
      * think of a stack like a stack of playing cards, and each time
      * you add a screen it slides in on top. Stacks can contain
      * other stacks, for example if you have a tab bar, each of the
      * tabs has its own individual stack. This is where the playing
      * card analogy falls apart, but it's still useful when thinking
      * of individual stacks.
      */
    return (
      <NavigationProvider router={Router}>
        <StackNavigation initialRoute={Router.getRoute('home')} />
      </NavigationProvider>
    );
  }
}

class HomeScreen extends React.Component {
  static route = {
    navigationBar: {
      visible: false,
    }
  }

  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      openTags: false,
      openAnon: false,
      openSearch: false,
      openInfoSelect: false,
      openConfirmation: false,
      dataSource: ds.cloneWithRows(posts)
    };
  }

  openControlPanel = () => {
    this._drawer.open()
  };

  render() {
    return (
      <Drawer
        type="static"
        content={
          <View style={styles.menuContainer}>
        <View style={styles.header}> 
          <Text style={{fontSize: 5}}> </Text>
        </View>
        <View style = {styles.nameContainer}>
          <Text style = {styles.userName}> Jane Doe </Text>
          <Text style = {styles.userInfo}> JaneDoe@stanford.edu </Text>
          <Text style = {styles.userInfoBottom}> Stanford, CA </Text>
        </View>
        <TouchableOpacity onPress={() => this.props.navigator.push(Router.getRoute('test'))}>
          <View style={styles.menuItemOne}> 
            <Text style={styles.menuItem}> Notifications </Text>
            <Image
              style ={styles.icon}
              source={require('./img/Notification.png')}
            />
          </View>
        </TouchableOpacity>
        <View style={styles.menuItemOne}> 
          <Text style={styles.menuItem}> My Posts </Text>
        </View>

        <View style={styles.channelLabel}> 
          <Text style={styles.menuItem}> Channels </Text>
          <Image
            style ={styles.icon}
            source={require('./img/AddChannel.png')}
          />
        </View>

        <TouchableOpacity onPress={() => this.props.navigator.push(Router.getRoute('home'))}>
          <View style={styles.menuItemTwo}> 
            <Text style={styles.menuItem}> General </Text>
          </View>
        </TouchableOpacity>
        <View style={styles.menuItemTwo}> 
          <Text style={styles.menuItem}> Stanford </Text>
        </View>
      </View>
        }
        openDrawerOffset={100}
        tapToClose={true}
        ref={(ref) => this._drawer = ref}
        tweenHandler={Drawer.tweenPresets.parallax}
        >

        <View style={styles.container}>
     
        {/*Header*/}
          <View style={styles.header}>
            <Button 
              style={styles.homeIconButton}
              onPress={()=> this.openControlPanel()}>
              <Image
                style ={styles.menuIcon}
                source={require('./img/hamburger-grey.png')}
              />
            </Button>
            <Image 
              style ={styles.resizeMode}
              resizeMode={Image.resizeMode.contain}
              source = {require('./img/FullLogo.png')}/>
            <Button 
              style={styles.searchIconButton}
              onPress={() => this.setState({openSearch: true, offset: -100})}>
              <Image
                style ={styles.searchBar}
                source={require('./img/search-grey.png')}/>
            </Button>
          </View>
          <View style={styles.banner}>
            <Text style={{color: '#7f7f7f', fontWeight: 'bold', fontSize: 18}}> General </Text>
          </View>

        {/*Newsfeed Container*/}
          <ScrollView>
            <ListView
              dataSource={this.state.dataSource}
              renderRow={(rowData) => <Post{...rowData} navigator={this.props.navigator}/>}/>
          </ScrollView>
        
        {/*Footer*/}
          <View style={styles.footer}>
              <AutogrowInput
                maxHeight={120}
                defaultHeight={30}
                {...this.props}
                multiline = {true}
                style={styles.input}
                placeholder="How are you feeling?"
                onChangeText = {(text) => this.setState({text})}
                value={this.state.text}/> 
               <Button
                style={styles.button}
                styleDisabled={{color: 'red'}}
                onPress={() => this.setState({openTags: true, offset: -100})}>
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center',}}>
                  <Image source= {require('./img/Submit.png')} style={styles.submitIcon} />
                </View>
              </Button>
          </View>
          <KeyboardSpacer/>

          <Modal
               offset={this.state.offset}
               open={this.state.openTags}
               modalDidOpen={() => console.log('modal did open')}
               modalDidClose={() => this.setState({openTags: false})}
               style={styles.modal}>
               <View>
                <TextInput
                          {...this.props}
                          style={styles.tag}
                          placeholder="List tags here"
                          onChangeText = {(text) => this.setState({text})}/>
                    <TextInput
                      {...this.props}
                      placeholder="Input a channel to post to"
                      onChangeText = {(text) => this.setState({text})}
                      style={styles.tag}/>
                    <Button
                      style={styles.button}
                      styleDisabled={{color: 'red'}}
                      onPress={() => this.setState({openTags: false, openConfirmation: true, text:""})}>
                      Post
                    </Button>
              </View>
            </Modal>

            {/*Anonymity Drop Request Modal*/}
            <Modal 
            	offset={this.state.offset}
            	open={this.state.openAnon}
            	modalDidOpen={() => console.log('other modal did open')}
            	modalDidClose={() => this.setState({openAnon: false})}
            	style={styles.modal}>
            	<View>
					<TextInput
              			{...this.props}
              			multiline = {true}
              			style={styles.largeInput}
              			placeholder="Let Anonymous Moose know why you want to exchange information"
              			onChangeText = {(text2) => this.setState({text2})}
              			value={this.state.text2}/>
                <Button
                      style={styles.button}
                      styleDisabled={{color: 'red'}}
                      onPress={() => this.setState({openAnon: false, openInfoSelect: true})}>
                      Add Message
                    </Button> 
              </View>
            </Modal>

              {/*Confirmation Modal*/}
              <Modal
                 offset={this.state.offset}
                 open={this.state.openConfirmation}
                 modalDidOpen={() => console.log('confirmation modal did open')}
                 modalDidClose={() => this.setState({openConfirmation: false})}
                 style={styles.modal}>
                  <Button
                    style={styles.button}
                    styleDisabled={{color: 'red'}}
                    onPress={() => this.setState({openConfirmation: false, text:""})}>
                    <Image source= {require('./img/confirmation.png')} style={styles.confirmation} />
                  </Button>
              </Modal>

              {/*Anonymoty Drop Info Share Modal*/}
              <Modal 
              	offset={this.state.offset}
              	open={this.state.openInfoSelect}
              	modalDidOpen={() => console.log('infoselectmodal did open')}
              	modalDidClose={() => this.setState({openInfoSelect: false})}
              	style={styles.modal}>
              	<View>
              		<View>
              			<View style={{flexDirection: 'row', backgroundColor: '#ff8b76', 
              			justifyContent: 'center', alignItems: 'center', marginBottom: 15}}>
  							<Text style={{fontWeight: 'bold', padding: 15}}>
  								Select Information to Share:</Text>
              			</View>
  						<CheckBox
  							label="Name"
  							onChange={(checked) => console.log('I am checked', checked)}
  						/>
  						<CheckBox
  							label="Location"
  							onChange={(checked) => console.log('I am checked', checked)}

  						/>
  						<CheckBox
  							label="Email"
  							onChange={(checked) => console.log('I am checked', checked)}
  						/>
              <View style={styles.blueLine}>
  						<Button
  							style={styles.button}
  							styleDisabled={{color: 'red'}}
                        		onPress={() => this.setState({openInfoSelect: false, text:""})}>
                        		Send Request
                        	</Button>
                        </View>
  	            	</View>         	
              	</View>
              </Modal>

            {/*Search Tags Modal*/}
              <Modal 
              	offset={this.state.offset}
              	open={this.state.openSearch}
              	modalDidOpen={() => console.log('search modal did open')}
              	modalDidClose={() => this.setState({openSearch: false})}
              	style={styles.modal}>
              	<View>
              		<TextInput
                        style={styles.tag}
                        {...this.props}
                        placeholder="View posts with tag:"
                        onChangeText = {(text) => this.setState({text})}
                        />
                      <View style={styles.blueLine}>
                      <Button
                        style={styles.button}
                        styleDisabled={{color: 'red'}}
                        onPress={() => this.setState({openSearch: false, text:""})}>
                        Search
                      </Button>
                      </View>
                </View>
              </Modal>
        </View>
      </Drawer>
    );
  }  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    //justifyContent: 'center',
  },
  icon: {
    width: 30,
    height: 30,
  },
  menuContainer: {
    flex: 1,
    backgroundColor: '#c6c6c6',
    //justifyContent: 'center',
  },
  nameContainer: {
    backgroundColor: '#B6E3F1',
    // borderWidth: 7,
    margin: 20,
    alignItems: 'center',
  },
  userName: {
    margin: 10,
    paddingTop: 10,
    fontSize: 20,
    fontWeight: 'bold', 
  },
  userInfo: {
    fontSize: 12,
    fontWeight: 'bold', 
  },
  userInfoBottom: {
    fontSize: 12,
    marginBottom: 20,
    fontWeight: 'bold', 
  },
  menuItemOne: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 5,
    backgroundColor: '#e9ebee',
  },
  menuItemTwo: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 5,
    backgroundColor: '#B6E3F1',
  },
  menuItem: {
    color: '#7f7f7f', 
    fontWeight: 'bold', 
    fontSize: 20,
  },
  channelLabel: {
    flexDirection: 'row',
    paddingTop: 25,
    paddingBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#B6E3F1',
  },
  footer: {
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#B6E3F1',
    flexDirection: 'row',
  },
  banner: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#e9ebee',
  },
  resizeMode: {
    width:150,
    height:50,
    marginTop: 10,
  },
  searchBar: {
    resizeMode: 'contain',
    width: 30,
    height: 30,
    marginTop: 15,
    marginLeft: 70,
  },
  menuIcon: {
    resizeMode: 'contain',
    width: 30,
    height: 30,
    marginTop: 15,
    marginRight: 70,
  },
  text: {
    marginLeft: 12,
    fontSize: 16,
  },
  photo: {
    height: 40,
    width: 40,
    borderRadius: 20,
  },
  input: {
    marginRight: 10,
    marginLeft: 10,
    height: 30,
    flex: 1,
    paddingHorizontal: 8,
    fontSize: 15,
    backgroundColor: '#FFFFFF',
    borderRadius: 2,
  },
  largeInput: {
    marginRight: 10,
    marginLeft: 10,
    height: 30,
    flex: 1,
    paddingHorizontal: 8,
    fontSize: 15,
    backgroundColor: '#FFFFFF',
    borderRadius: 2,
    paddingBottom: 200,
  },
  searchIconButton:{
    width: 30,
    height: 30,
    marginTop: 15,
    marginLeft: 70,
  },
  homeIconButton:{
    width: 30,
    height: 30,
    marginTop: 15,
    marginRight: 70,
  },
  submitIcon:{
    marginTop: 5,
    marginRight: 10,
    height: 30,
    width: 30,
  },
  confirmation:{
    alignItems: 'center',
    height: 200,
    width: 240,
    margin: 10,
  },
  button: {
    margin: 10,
    padding: 7,
    alignItems: 'center',
    justifyContent: 'center',
    color: '#696969',
    backgroundColor: '#b7e3f1',
  },
  modal: {
    borderRadius: 10,
    margin: 20,
    padding: 10,
    backgroundColor: '#d5d5d5'
  },
  tag: {
    margin: 10,
    height: 30,
    flex: 1,
    paddingHorizontal: 8,
    fontSize: 15,
    backgroundColor: '#FFFFFF',
    borderRadius: 2,
  },
});

AppRegistry.registerComponent('main', () => App);
Exponent.registerRootComponent(App);

export default HomeScreen;