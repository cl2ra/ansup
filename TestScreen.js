import Exponent from 'exponent';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import React from 'react';
import { withNavigation } from '@exponent/ex-navigation';
import Post from './post.js';
import Button from 'react-native-button';
import Modal from 'react-native-simple-modal';
import CheckBox from 'react-native-checkbox';
import AutogrowInput from 'react-native-autogrow-input';
import Drawer from 'react-native-drawer';
import HomeScreen from './main'

import { 
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  div,
  ScrollView,
  ListView,
  TextInput,
  TouchableOpacity 
} from 'react-native';

import {
  createRouter,
  NavigationProvider,
  StackNavigation,
} from '@exponent/ex-navigation';

const Router = createRouter(() => ({
  test: () => TestScreen,
  home: () => HomeScreen,
}));

{/* Turning this test screen into the notifications screen!! */}

@withNavigation
class TestScreen extends React.Component {

  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      openRevealRequest: false,
      openTags: false,
      openAnon: false,
      openSearch: false,
      openInfoSelect: false,
      notificationPressed: false,
      openRevealedInfo: false,
    };
  }

  /*<View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
    <Text onPress={this._goBack}>Go back</Text>
  </View>*/


    openControlPanel = () => {
      this._drawer.open()
    };

  render() {
    var navigationView = (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <Text style={{margin: 10, fontSize: 15, textAlign: 'left'}}>In the Drawer!</Text>
      </View>
    );

    return (
      <Drawer
        type="static"
        content={
          <View style={styles.menuContainer}>
        <View style={styles.header}> 
          <Text style={{fontSize: 5}}> </Text>
        </View>
        <View style = {styles.nameContainer}>
          <Text style = {styles.userName}> Jane Doe </Text>
          <Text style = {styles.userInfo}> JaneDoe@stanford.edu </Text>
          <Text style = {styles.userInfoBottom}> Stanford, CA </Text>
        </View>
        <TouchableOpacity onPress={() => this.props.navigator.push(Router.getRoute('test'))}>
          <View style={styles.menuItemOne}> 
            <Text style={styles.menuItem}> Notifications </Text>
          </View>
        </TouchableOpacity>
        <View style={styles.menuItemOne}> 
          <Text style={styles.menuItem}> My Posts </Text>
        </View>

        <View style={styles.channelLabel}> 
          <Text style={styles.menuItem}> Channels </Text>
          <Image
            style ={styles.icon}
            source={require('./img/AddChannel.png')}
          />
        </View>

        <TouchableOpacity onPress={() => this.props.navigator.push(Router.getRoute('home'))}>
        <View style={styles.menuItemTwo}> 
          <Text style={styles.menuItem}> General </Text>
        </View>
        </TouchableOpacity>

        <View style={styles.menuItemTwo}> 
          <Text style={styles.menuItem}> Stanford </Text>
        </View>
      </View>
        }
        openDrawerOffset={100}
        tapToClose={true}
        ref={(ref) => this._drawer = ref}
        tweenHandler={Drawer.tweenPresets.parallax}
        >

      <View style={styles.container}>
        <View style={styles.header}>
          <Button 
            style={styles.homeIconButton}
            onPress={()=> this.openControlPanel()}>
            <Image
              style ={styles.menuIcon}
              source={require('./img/hamburger-grey.png')}
            />
          </Button>
          <Button
            onPress={() => this.props.navigator.push(Router.getRoute('home'))}>
            <Image 
              style ={styles.resizeMode}
              resizeMode={Image.resizeMode.contain}
              source = {require('./img/FullLogo.png')}/>
          </Button>
          <Button 
            style={styles.searchIconButton}
            onPress={() => this.setState({openSearch: true, offset: -100})}>
            <Image
              style ={styles.searchBar}
              source={require('./img/search-grey.png')}/>
          </Button>
        </View>
        <View style={styles.banner}>
            <Text style={{color: '#7f7f7f', fontWeight: 'bold', fontSize: 18}}> Notifications </Text>        
        </View>

        <ScrollView>
          <View style={styles.postContainer}>
              <Text style={[styles.textPost, this.state.notificationPressed ? {fontWeight: 'normal'} : {}]}>
                You have a new reveal request!
              </Text>
          </View>
              <Button
                style={styles.button}
                onPress={() => this.setState({openRevealRequest: true, offset: -100, notificationPressed: true})}>
                View Request
              </Button>
        </ScrollView>
         


            <Modal 
              offset={this.state.offset}
              open={this.state.openRevealRequest}
              modalDidOpen={() => console.log('reveal request did open')}
              modalDidClose={() => this.setState({openRevealRequest: false})}
              style={styles.modal}>
              <View>
                <Text style={styles.revealRequest}>
                  I know it can sometimes be hard to form new relationships--I feel exactly the same way and I think a lot of 
                  people do too. Youre not alone. Would you like to meet in person and talk sometime? It might be good to 
                  see someone with the same issues face-to-face.
                </Text>
                <Button
                      onPress={() => this.setState({openInfoSelect: true, openRevealRequest: false, offset: -100})}
                      style={styles.button}>
                      Reveal Identity
                    </Button> 
                <Button
                  style={styles.declineButton}
                  onPress={() => this.setState({openRevealRequest: false})}>
                  Decline Reveal
                </Button>   
              </View>
            </Modal>


            <Modal 
              offset={this.state.offset}
              open={this.state.openInfoSelect}
              modalDidOpen={() => console.log('infoselectmodal did open')}
              modalDidClose={() => this.setState({openInfoSelect: false})}
              style={styles.modal}>
              <View>
                <View>
                  <View style={{flexDirection: 'column', backgroundColor: '#ff8b76', 
                  justifyContent: 'center', alignItems: 'center', margin: 10, marginBottom: 15}}>
              <Text style={{flexDirection: 'row', fontWeight: 'bold', padding: 15}}>
                Select Information to Share: </Text>
              <Text style={{flexDirection: 'row', padding: 15}}>
                Commenter offered to share their name, location, and email</Text>
                  </View>
            <View style={{margin: 10}}>
            <CheckBox
              label="Name"
              onChange={(checked) => console.log('I am checked', checked)}
            />
            <CheckBox
              label="Location"
              onChange={(checked) => console.log('I am checked', checked)}

            />
            <CheckBox 
              label="Email"
              onChange={(checked) => console.log('I am checked', checked)}
            />
            </View>
            <Button
              style={styles.button}
              styleDisabled={{color: 'red'}}
                          onPress={() => this.setState({openInfoSelect: false, openRevealedInfo: true, text:""})}>
                          Exchange Information
                        </Button>
                </View>           
              </View>
            </Modal>

            <Modal
              offset={this.state.offset}
              open={this.state.openRevealedInfo}
              modalDidOpen={() => console.log('info modal did open')}
              modalDidClose={() => this.setState({openRevealedInfo: false})}
              style={styles.modal}>
              <View style={{flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                <Text>Name: John Smith</Text>
                <Text>Email: jsmith18@stanford.edu</Text>
                <Text>Location: Stanford, CA</Text>
              </View>
            </Modal>

            <Modal 
              offset={this.state.offset}
              open={this.state.openAnon}
              modalDidOpen={() => console.log('other modal did open')}
              modalDidClose={() => this.setState({openAnon: false})}
              style={styles.modal}>
              <View>
          <TextInput
                    {...this.props}
                    multiline = {true}
                    style={styles.largeInput}
                    placeholder="Let Anonymous Moose know why you want to exchange information"
                    onChangeText = {(text2) => this.setState({text2})}
                    value={this.state.text2}/>
                <Button
                      style={styles.button}
                      styleDisabled={{color: 'red'}}
                      onPress={() => this.setState({openAnon: false, openInfoSelect: true})}>
                      Add Message
                    </Button>  
              </View>
            </Modal>
            <Modal 
              offset={this.state.offset}
              open={this.state.openSearch}
              modalDidOpen={() => console.log('search modal did open')}
              modalDidClose={() => this.setState({openSearch: false})}
              style={styles.modal}>
              <View>
                <TextInput
                      {...this.props}
                      style={styles.tag}
                      placeholder="View posts with tag:"
                      onChangeText = {(text) => this.setState({text})}/>
                    <Button
                      style={styles.button}
                      styleDisabled={{color: 'red'}}
                      onPress={() => this.setState({openSearch: false, text:""})}>
                      Search
                    </Button>
              </View>
            </Modal>
        </View>

      </Drawer>
    );
  }  

  _goBack = () => {
    if (this.props.navigator.getCurrentIndex() > 0) {
      this.props.navigator.pop();
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    //justifyContent: 'center',
  },
  icon: {
    width: 30,
    height: 30,
  },
  menuContainer: {
    flex: 1,
    backgroundColor: '#c6c6c6',
    //justifyContent: 'center',
  },
  nameContainer: {
    backgroundColor: '#B6E3F1',
    // borderWidth: 7,
    margin: 20,
    alignItems: 'center',
  },
  userName: {
    margin: 10,
    paddingTop: 10,
    fontSize: 20,
    fontWeight: 'bold', 
  },
  userInfo: {
    fontSize: 12,
    fontWeight: 'bold', 
  },
  userInfoBottom: {
    fontSize: 12,
    marginBottom: 20,
    fontWeight: 'bold', 
  },
  menuItemOne: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 5,
    backgroundColor: '#e9ebee',
  },
  menuItemTwo: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 5,
    backgroundColor: '#B6E3F1',
  },
  menuItem: {
    color: '#7f7f7f', 
    fontWeight: 'bold', 
    fontSize: 20,
  },
  channelLabel: {
    flexDirection: 'row',
    paddingTop: 25,
    paddingBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  banner: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#e9ebee',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#B6E3F1',
  },
  resizeMode: {
    width:150,
    height:50,
    marginTop: 10,
  },
  searchBar: {
    resizeMode: 'contain',
    width: 30,
    height: 30,
    marginTop: 15,
    marginLeft: 70,
  },
  menuIcon: {
    resizeMode: 'contain',
    width: 30,
    height: 30,
    marginTop: 15,
    marginRight: 70,
  },
  text: {
    marginLeft: 12,
    fontSize: 16,
  },
  postContainer:{
    flex: 1,
    flexWrap: 'wrap',
    paddingTop: 15,
    flexDirection: 'column',
    alignItems: 'center',
  },
  textBorder: {
    borderBottomColor: '#b7e3f1',
    borderBottomWidth: 1,
  },
  textPost: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  ViewButton: {
    marginTop: 5,
    alignItems: 'center',
    justifyContent: 'center',
    color: '#696969',
  },
  button: {
    margin: 10,
    padding: 7,
    alignItems: 'center',
    justifyContent: 'center',
    color: '#696969',
    backgroundColor: '#b7e3f1',
  },
  declineButton: {
    margin: 10,
    padding: 7,
    alignItems: 'center',
    justifyContent: 'center',
    color: '#696969',
    backgroundColor: '#ff8b76',
  },
  actionables: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  largeInput: {
    marginRight: 10,
    marginLeft: 10,
    height: 30,
    flex: 1,
    paddingHorizontal: 8,
    fontSize: 15,
    backgroundColor: '#FFFFFF',
    borderRadius: 2,
    paddingBottom: 200,
  },
  searchIconButton:{
    width: 30,
    height: 30,
    marginTop: 15,
    marginLeft: 70,
  },
  homeIconButton:{
    width: 30,
    height: 30,
    marginTop: 15,
    marginRight: 70,
  },
  revealRequest: {
    margin: 15,
    fontSize: 16,
    color: '#7f7f7f'
  },
  modal: {
    borderRadius: 2,
    margin: 20,
    padding: 10,
    backgroundColor: '#d5d5d5'
  },
  tag: {
    margin: 10,
    height: 30,
    flex: 1,
    paddingHorizontal: 8,
    fontSize: 15,
    backgroundColor: '#FFFFFF',
    borderRadius: 2,
  },
});

export default TestScreen;
