import Exponent from 'exponent';
import React from 'react';
import StackView from 'react-stack-view';
import Button from 'react-native-button';
import Modal from 'react-native-simple-modal';
import TestScreen from './TestScreen';
import Comments from './comments';


import { 
	View, Text, ListView, StyleSheet, Image, TouchableOpacity,
} from 'react-native';


var userImages = [
require("./img/users/giraffe.jpg"),
require("./img/users/lion.jpg"),
require("./img/users/moose.jpg"),
require("./img/users/penguin.jpg"),
require("./img/users/squirrel.jpg"),
require("./img/users/zebra.jpg"),
]


//Post code based on code from:
//https://medium.com/differential/react-native-basics-how-to-use-the-listview-component-a0ec44cf1fe8#.h2gy9wph3

const CommentForm = (props) => (
  <View style={styles.commentContainer}>
      <Image 
        source={userImages[props.index]} 
        style= {styles.photo} >
        </Image>
        <Text style={styles.textPost}>
        {`${props.text}`}
      </Text>
    <View style={styles.actionables}>
      <Image source= {require('./img/HeartNoFill.png')} style={styles.heart} />
      <Text style = {styles.heartText}>{`${props.upvotes}`}</Text>
    </View>
  </View>
);


const styles = StyleSheet.create({
  commentContainer:{
    flex: 1,
    flexWrap: 'wrap',
    padding: 12,
    flexDirection: 'column',
    borderLeftColor: '#e9ebee',
    borderLeftWidth:25,
    borderBottomColor: '#e9ebee',
    borderBottomWidth: 5,
    borderRightColor: '#e9ebee',
    borderRightWidth: 5,
    marginLeft: 3,
  },
  textBorder: {
    borderBottomColor: '#b7e3f1',
    borderBottomWidth: 1,
  },
  tags: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#696969',
  },
   textPost: {
    fontSize: 16,
    fontWeight: 'normal',
    marginBottom: 10,
    marginLeft: 40,
  },
  photo: {
    height: 40,
    width: 40,
    borderRadius: 20,
  },
  heart: {
    marginTop: 5,
    height: 25,
    width: 28,
  },
  speechIcon:{
    marginTop: 5,
    marginLeft: 50,
    height: 25,
    width: 30,
  },
  supportButton: {
    padding: 6,
    marginTop: 5,
    marginRight:50,
    color: '#696969',
  },
  actionables: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  heartText: {
    fontSize: 20,
    marginRight: 10,
    marginLeft: 3,
    marginTop: 10,
    color: '#696969',
  },
});

export default CommentForm;