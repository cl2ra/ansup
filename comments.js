import Exponent from 'exponent';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import React from 'react';
import { withNavigation } from '@exponent/ex-navigation';
import CommentForm from './commentFormat.js';
import Button from 'react-native-button';
import Modal from 'react-native-simple-modal';
import CheckBox from 'react-native-checkbox';
import AutogrowInput from 'react-native-autogrow-input';
import Drawer from 'react-native-drawer';
import TestScreen from './TestScreen';
import HomeScreen from './main';

import { 
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  div,
  ScrollView,
  ListView,
  TextInput,
  TouchableOpacity 
} from 'react-native';

import {
  createRouter,
  NavigationProvider,
  StackNavigation,
} from '@exponent/ex-navigation';

const Router = createRouter(() => ({
  test: () => TestScreen,
  home: () => HomeScreen,
}));

var userImages = [
require("./img/users/giraffe.jpg"),
require("./img/users/lion.jpg"),
require("./img/users/moose.jpg"),
require("./img/users/penguin.jpg"),
require("./img/users/squirrel.jpg"),
require("./img/users/zebra.jpg"),
]

{/* Turning this test screen into the comments screen!! */}

@withNavigation
class Comments extends React.Component {

  constructor(props) {
    super(props);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    this.state = {
      openTags: false,
      openAnon: false,
      openSearch: false,
      openInfoSelect: false,
      openComment: false,
      dataSource: ds.cloneWithRows(this.props.route.params.comments),
    };
  }

  /*<View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
    <Text onPress={this._goBack}>Go back</Text>
  </View>*/

  openControlPanel = () => {
    this._drawer.open()
  };


  render() {
    return (
            <Drawer
        type="static"
        content={
          <View style={styles.menuContainer}>
        <View style={styles.header}> 
          <Text style={{fontSize: 5}}> </Text>
        </View>
        <View style = {styles.nameContainer}>
          <Text style = {styles.userName}> Jane Doe </Text>
          <Text style = {styles.userInfo}> JaneDoe@stanford.edu </Text>
          <Text style = {styles.userInfoBottom}> Stanford, CA </Text>
        </View>
        <TouchableOpacity onPress={() => this.props.navigator.push(Router.getRoute('test'))}>
          <View style={styles.menuItemOne}> 
            <Text style={styles.menuItem}> Notifications </Text>
            <Image
              style ={styles.icon}
              source={require('./img/Notification.png')}
            />
          </View>
        </TouchableOpacity>
        <View style={styles.menuItemOne}> 
          <Text style={styles.menuItem}> My Posts </Text>
        </View>

        <View style={styles.channelLabel}> 
          <Text style={styles.menuItem}> Channels </Text>
          <Image
            style ={styles.icon}
            source={require('./img/AddChannel.png')}
          />
        </View>

        <TouchableOpacity onPress={() => this.props.navigator.push(Router.getRoute('home'))}>
          <View style={styles.menuItemTwo}> 
            <Text style={styles.menuItem}> General </Text>
          </View>
        </TouchableOpacity>
        <View style={styles.menuItemTwo}> 
          <Text style={styles.menuItem}> Stanford </Text>
        </View>
      </View>
        }
        openDrawerOffset={100}
        tapToClose={true}
        ref={(ref) => this._drawer = ref}
        tweenHandler={Drawer.tweenPresets.parallax}
        >


      <View style={styles.container}>
   
          <View style={styles.header}>
            <Button 
              style={styles.homeIconButton}
              onPress={()=> this.openControlPanel()}>
              <Image
                style ={styles.menuIcon}
                source={require('./img/hamburger-grey.png')}
              />
            </Button>
            <Image 
              style ={styles.resizeMode}
              resizeMode={Image.resizeMode.contain}
              source = {require('./img/FullLogo.png')}/>
            <Button 
              style={styles.searchIconButton}
              onPress={() => this.setState({openSearch: true, offset: -100})}>
              <Image
                style ={styles.searchBar}
                source={require('./img/search-grey.png')}/>
            </Button>
          </View>
        <View style={styles.banner}>
            <Text 
              style={{color: '#7f7f7f', fontWeight: 'bold', fontSize: 18, marginLeft: 15}}
              onPress={this._goBack}>
                {'<'} General 
            </Text>
        </View>

        <ScrollView>
        <View style={styles.postContainer}>
          <View style = {styles.actionablesPost}>
          <Button style={{marginRight: 20}}
            onPress={() => this.setState({openAnon: true, offset: -100})}>
            <Image 
              source = {userImages[`${this.props.route.params.index}`]} 
              style= {styles.photo} />
          </Button>

          <Text style={styles.tags}>
            {`${this.props.route.params.tags}`}
          </Text>
          </View>
          <View style = {styles.textBorder}>
            <Text style={styles.textPost}>
              {`${this.props.route.params.postText}`}
            </Text>
          </View>
          <View style = {styles.actionables}>
            <Image source= {require('./img/HeartHand.png')} style={styles.heartHand} />
            <Button
              style={styles.supportButton}
              styleDisabled={{color: 'red'}}
              onPress={() => this.setState({openComment: true, offset: -100})}>
              Support
            </Button>
            <Image source= {require('./img/HeartNoFill.png')} style={styles.heart} />
            <Text style = {styles.heartText}>{`${this.props.route.params.upvotes}`}</Text>
          </View>
        </View>

        <ListView
              dataSource={this.state.dataSource}
              renderRow={(rowData) => <CommentForm{...rowData} navigator={this.props.navigator}/>}/>
         
        </ScrollView>
         
            <Modal 
              offset={this.state.offset}
              open={this.state.openComment}
              modalDidOpen={() => console.log('other modal did open')}
              modalDidClose={() => this.setState({openComment: false})}
              style={styles.modal}>
              <View>
              <TextInput
                    {...this.props}
                    multiline = {true}
                    style={styles.largeInput}
                    placeholder={"Share your thoughts with Anonymous "+ this.props.route.params.name +"."}
                    onChangeText = {(text2) => this.setState({text2})}
                    value={this.state.text2}/>
                <Button
                  style={styles.button}
                  styleDisabled={{color: 'red'}}
                  onPress={() => this.setState({openComment: false})}>
                  Submit Comment
                </Button>             
              </View>
            </Modal>
            <Modal 
              offset={this.state.offset}
              open={this.state.openAnon}
              modalDidOpen={() => console.log('other modal did open')}
              modalDidClose={() => this.setState({openAnon: false})}
              style={styles.modal}>
              <View>
              <TextInput
                    {...this.props}
                    multiline = {true}
                    style={styles.largeInput}
                    placeholder={"Let Anonymous "+ this.props.route.params.name +" know why you want to exchange information"}
                    onChangeText = {(text2) => this.setState({text2})}
                    value={this.state.text2}/>
                <Button
                      style={styles.button}
                      styleDisabled={{color: 'red'}}
                      onPress={() => this.setState({openAnon: false, openInfoSelect: true})}>
                      Add Message
                    </Button>   
              </View>
            </Modal>
            <Modal 
              offset={this.state.offset}
              open={this.state.openSearch}
              modalDidOpen={() => console.log('search modal did open')}
              modalDidClose={() => this.setState({openSearch: false})}
              style={styles.modal}>
              <View>
                <TextInput
                      {...this.props}
                      style={styles.tag}
                      placeholder="View posts with tag:"
                      onChangeText = {(text) => this.setState({text})}/>
                    <Button
                      style={styles.button}
                      styleDisabled={{color: 'red'}}
                      onPress={() => this.setState({openSearch: false, text:""})}>
                      Search
                    </Button>
              </View>
            </Modal>

            <Modal 
              offset={this.state.offset}
              open={this.state.openInfoSelect}
              modalDidOpen={() => console.log('infoselectmodal did open')}
              modalDidClose={() => this.setState({openInfoSelect: false})}
              style={styles.modal}>
              <View>
                <View>
                  <View style={{flexDirection: 'column', backgroundColor: '#ff8b76', 
                  justifyContent: 'center', alignItems: 'center', margin: 10, marginBottom: 15}}>
              <Text style={{flexDirection: 'row', fontWeight: 'bold', padding: 15}}>
                Select Information to Share: </Text>
                  </View>
            <View style={{margin: 10}}>
            <CheckBox
              label="Name"
              onChange={(checked) => console.log('I am checked', checked)}
            />
            <CheckBox
              label="Location"
              onChange={(checked) => console.log('I am checked', checked)}

            />
            <CheckBox 
              label="Email"
              onChange={(checked) => console.log('I am checked', checked)}
            />
            </View>
            <Button
              style={styles.button}
              styleDisabled={{color: 'red'}}
                          onPress={() => this.setState({openInfoSelect: false})}>
                          Exchange Information
                        </Button>
                </View>           
              </View>
            </Modal>
      </View>

      </Drawer>
    );
  }  

  _goBack = () => {
    if (this.props.navigator.getCurrentIndex() > 0) {
      this.props.navigator.pop();
    }
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    //justifyContent: 'center',
  },
    icon: {
    width: 30,
    height: 30,
  },
  menuContainer: {
    flex: 1,
    backgroundColor: '#c6c6c6',
    //justifyContent: 'center',
  },
  nameContainer: {
    backgroundColor: '#B6E3F1',
    // borderWidth: 7,
    margin: 20,
    alignItems: 'center',
  },
  userName: {
    margin: 10,
    paddingTop: 10,
    fontSize: 20,
    fontWeight: 'bold', 
  },
  userInfo: {
    fontSize: 12,
    fontWeight: 'bold', 
  },
  userInfoBottom: {
    fontSize: 12,
    marginBottom: 20,
    fontWeight: 'bold', 
  },
  menuItemOne: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 5,
    backgroundColor: '#e9ebee',
  },
  menuItemTwo: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 5,
    backgroundColor: '#B6E3F1',
  },
  menuItem: { 
    fontWeight: 'bold', 
    fontSize: 20,
    color: '#7f7f7f'
  },
  channelLabel: {
    flexDirection: 'row',
    paddingTop: 25,
    paddingBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  banner: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#e9ebee',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#B6E3F1',
  },
  resizeMode: {
    width:150,
    height:50,
    marginTop: 10,
  },
  searchBar: {
    resizeMode: 'contain',
    width: 30,
    height: 30,
    marginTop: 15,
    marginLeft: 70,
  },
  menuIcon: {
    resizeMode: 'contain',
    width: 30,
    height: 30,
    marginTop: 15,
    marginRight: 70,
  },
  text: {
    marginLeft: 12,
    fontSize: 16,
  },
  photo: {
    height: 40,
    width: 40,
    borderRadius: 20,
    marginRight:20,
  },
  input: {
    marginRight: 10,
    marginLeft: 10,
    height: 30,
    flex: 1,
    paddingHorizontal: 8,
    fontSize: 15,
    backgroundColor: '#FFFFFF',
    borderRadius: 2,
  },
  largeInput: {
    marginRight: 10,
    marginLeft: 10,
    height: 30,
    flex: 1,
    paddingHorizontal: 8,
    fontSize: 15,
    backgroundColor: '#FFFFFF',
    borderRadius: 2,
    paddingBottom: 200,
  },
  searchIconButton:{
    width: 30,
    height: 30,
    marginTop: 15,
    marginLeft: 70,
  },
  homeIconButton:{
    width: 30,
    height: 30,
    marginTop: 15,
    marginRight: 70,
  },
  button: {
    margin: 10,
    padding: 7,
    alignItems: 'center',
    justifyContent: 'center',
    color: '#696969',
    backgroundColor: '#b7e3f1',
  },
  modal: {
    borderRadius: 2,
    margin: 20,
    padding: 10,
    backgroundColor: '#d5d5d5'
  },
  tag: {
    margin: 10,
    height: 30,
    flex: 1,
    paddingHorizontal: 8,
    fontSize: 15,
    backgroundColor: '#FFFFFF',
    borderRadius: 2,
  },
  postContainer:{
    flex: 1,
    flexWrap: 'wrap',
    padding: 12,
    flexDirection: 'column',
    alignItems: 'flex-start',
    borderColor: '#e9ebee',
    borderBottomWidth: 7,
    marginLeft: 3,
  },
  textBorder: {
    borderBottomColor: '#b7e3f1',
    borderBottomWidth: 1,
  },
  tags: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#696969',
  },
   textPost: {
    fontSize: 16,
    fontWeight: 'normal',
    marginBottom: 10,
  },
  // photo: {
  //   height: 40,
  //   width: 40,
  //   borderRadius: 20,
  // },
  heart: {
    marginTop: 5,
    marginLeft:80,
    height: 25,
    width: 28,
  },
  heartHand: {
    marginTop: 5,
    height: 30,
    width: 28,
  },
  speechIcon:{
    marginTop: 5,
    marginLeft: 50,
    height: 25,
    width: 30,
  },
  supportButton: {
    paddingTop: 6,
    paddingBottom: 6,
    paddingLeft:6,
    paddingRight:6,
    marginTop: 5,
    marginRight:80,
    color: '#696969',
  },
  actionables: {
    flex: 1,
    flexWrap: 'nowrap',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  actionablesPost: {
    flex: 1,
    flexWrap: 'nowrap',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',

  },
  heartText: {
    fontSize: 20,
    marginLeft: 3,
    marginTop: 10,
    color: '#696969',
  },
});

export default Comments;
