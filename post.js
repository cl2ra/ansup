import Exponent from 'exponent';
import React from 'react';
import StackView from 'react-stack-view';
import Button from 'react-native-button';
import Modal from 'react-native-simple-modal';
import TestScreen from './TestScreen';
import Comments from './comments';


import { 
	View, Text, ListView, StyleSheet, Image, TouchableOpacity,
} from 'react-native';

const Router = createRouter(() => ({
  test: () => TestScreen,
  home: () => HomeScreen,
  comments: () => Comments,
}));

var userImages = [
require("./img/users/giraffe.jpg"),
require("./img/users/lion.jpg"),
require("./img/users/moose.jpg"),
require("./img/users/penguin.jpg"),
require("./img/users/squirrel.jpg"),
require("./img/users/zebra.jpg"),
]

import {
  createRouter,
  NavigationProvider,
  StackNavigation,
} from '@exponent/ex-navigation';

//Post code based on code from:
//https://medium.com/differential/react-native-basics-how-to-use-the-listview-component-a0ec44cf1fe8#.h2gy9wph3

const Post = (props) => (
  <View style={styles.postContainer}>
    <View style = {styles.actionables}>
        <Image 
          source={userImages[`${props.index}`]} 
          style= {styles.photo} >
        </Image>
      <Text style={styles.tags}>
        {`${props.tags}`}
      </Text>
    </View>
      <View style = {styles.textBorder}>
        <Text style={styles.textPost}>
          {`${props.postText}`}
        </Text>
      </View>
    <View style = {styles.actionables}>
      <Image source= {require('./img/Speech.png')} style={styles.speechIcon} />
      <Button
        style={styles.supportButton}
        styleDisabled={{color: 'red'}}
        onPress={() => props.navigator.push(Router.getRoute('comments', {
          name: props.name,
          index: props.index, 
          tags: props.tags, 
          postText: props.postText, 
          upvotes: props.upvotes, 
          comments: props.comments}))}>
        Show your support
      </Button>
      <Image source= {require('./img/HeartNoFill.png')} style={styles.heart} />
      <Text style = {styles.heartText}>{`${props.upvotes}`}</Text>
    </View>
  </View>
);

_goBack = () => {
    if (this.props.navigator.getCurrentIndex() > 0) {
      this.props.navigator.pop();
    }
  }

const styles = StyleSheet.create({
  postContainer:{
    flex: 1,
    flexWrap: 'wrap',
    padding: 12,
    alignItems: 'flex-start',
    flexDirection: 'column',
    borderBottomColor: '#e9ebee',
    borderBottomWidth: 10,
    marginLeft: 3,
  },
  textBorder: {
    borderBottomColor: '#b7e3f1',
    borderBottomWidth: 1,
  },
  tags: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#696969',
  },
   textPost: {
    fontSize: 16,
    fontWeight: 'normal',
    marginBottom: 10,
  },
  photo: {
    height: 40,
    width: 40,
    borderRadius: 20,
    marginRight:20,
  },
  heart: {
    marginTop: 5,
    height: 25,
    width: 28,
  },
  speechIcon:{
    marginTop: 5,
    marginLeft: 50,
    height: 25,
    width: 30,
  },
  supportButton: {
    padding: 6,
    marginTop: 5,
    marginRight: 50,
    color: '#696969',
  },
  actionables: {
    flex: 1,
    flexWrap: 'nowrap',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',

  },
  heartText: {
    fontSize: 20,
    marginLeft: 3,
    marginTop: 10,
    color: '#696969',
  },
});

export default Post;