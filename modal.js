import Exponent from 'exponent';
import React from 'react';
import Modal from 'react-native-simple-modal';
import {
  StyleSheet,
  Text,
  View,
  Image,
  div,
  ScrollView,
  ListView,
  TextInput,
} from 'react-native';


const tagsModal =  () =>{
	<View>
		<Text>"Select up to 5 tags below!"</Text>
		<TextInput
              {...this.props}
              style={styles.tag}
        />
        <TextInput
          {...this.props}
          style={styles.tag}
        />
        <TextInput
          {...this.props}
          style={styles.tag}
        />
        <TextInput
          {...this.props}
          style={styles.tag}
        />
        <TextInput
          {...this.props}
          style={styles.tag}
        />
        <Text>"Choose a channel to post to:"</Text>
        <TextInput
          {...this.props}
          style={styles.tag}
        />

	</View>
};

const styles = StyleSheet.create({

  tag: {
    marginRight: 10,
    marginLeft: 10,
    height: 30,
    flex: 1,
    paddingHorizontal: 8,
    fontSize: 15,
    backgroundColor: '#FFFFFF',
    borderRadius: 2,
  },

});

export default Modal;